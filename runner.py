#!/usr/bin/env python3.4


from http_server import run_server
from proxy import ProxyServer

if __name__ == '__main__':
    run_server()
    server = ProxyServer()
    server._bind()
    server.main()
