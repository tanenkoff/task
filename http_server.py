

from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
import argparse


class HttpServer(BaseHTTPRequestHandler):
    def get_method(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        test_text = " HELLO "
        self.wfile.write(bytes(test_text, "utf8"))
        return


def run_server():
    print('Starting...')
    parser = argparse.ArgumentParser()
    parser.add_argument('-P', '--Port', help='HTTP server port', default=8080)
    args = parser.parse_args()
    port = args.Port
    server_address = ('127.0.0.1', int(port))
    httpd = HTTPServer(server_address, HttpServer)
    print('running on ', server_address)
    httpd.serve_forever()



